import { HOST, TOKEN_NAME } from './../_shared/var.constant';
import { Paciente } from './../_model/paciente';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PacienteService {

  url: string = `${HOST}/paciente`;
  pacienteCambio = new Subject<Paciente[]>();
  mensaje = new Subject<string>();

  constructor(private http: HttpClient) {

    /*let p = new Paciente();
    p.idPaciente = 1;
    p.nombres = "Hector";
    p.apellidos = "Garcia";
    this.pacientes.push(p);

    p = new Paciente();
    p.idPaciente = 2;
    p.nombres = "Joselyne";
    p.apellidos = "Vallejo";
    this.pacientes.push(p);*/
  }

  getListarTodos(p: number, s: number) {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Paciente[]>(`${this.url}/listarPagina?page=${p}&size=${s}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
    //return this.pacientes;
  }

  getPorId(id: number) {
    return this.http.get<Paciente>(`${this.url}/listar/${id}`);
  }

  registrar(paciente: Paciente) {
    return this.http.post(`${this.url}/registrar`, paciente);
  }

  actualizar(paciente: Paciente) {
    return this.http.put(`${this.url}/actualizar`, paciente);
  }

  eliminar(paciente: Paciente) {
    return this.http.delete(`${this.url}/eliminar/${paciente.idPaciente}`);
  }

}
