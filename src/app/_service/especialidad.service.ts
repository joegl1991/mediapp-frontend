import { HOST, TOKEN_NAME } from './../_shared/var.constant';
import { Especialidad } from './../_model/especialidad';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class EspecialidadService {

  url: string = `${HOST}/especialidad`;

  constructor(private http: HttpClient) { }

  getListarTodos() {
    let access_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.http.get<Especialidad[]>(`${this.url}/listar`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

}
